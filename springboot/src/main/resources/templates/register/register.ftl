<#import "/spring.ftl" as spring />
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<title>Connection</title>
	<link href="<@spring.url relativeUrl="/css/login-styles.css"/>" rel="stylesheet">
	<link href="<@spring.url relativeUrl="/css/bootstrap/bootstrap.css"/>" rel="stylesheet">
	<link rel="stylesheet" href="<@spring.url relativeUrl="/css/font-awesome.min.css"/>">
</head>
<body>
<!-- Where all the magic happens -->
<!-- LOGIN FORM -->
<div class="text-center" style="padding:50px 0">
	<div class="logo">Inscription</div>
	<!-- Main Form -->
	<div class="login-form-1">
		<form method="post" action="<@spring.url relativeUrl="/register/action"/>">
		<div class="form-group">
			<@spring.bind path="user"/>
			<@spring.formHiddenInput "user.id"/>
			<div class="login-group">
				<div class="form-group"><@spring.formInput "user.name" 'class="form-control" placeholder="Name" required'/></div>
			</div>
			<div class="login-group">
				<div class="form-group"><@spring.formInput "user.email" 'class="form-control" placeholder="E-mail" required'/></div>
			</div>
			<div class="login-group">
				<div class="form-group"><@spring.formPasswordInput "user.password" 'class="form-control" placeholder="Password" required'/></div>
			</div>
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
			<input type="submit" value="Ok" class="btn btn-success">
		</div>
	</form>
	<div class="clearboth"></div>
	<#if (dataIntegrityError ??)>
		<p style="color:red;">${dataIntegrityError}</p>
	</#if>
	</div>
	<!-- end:Main Form -->
</div>
</body>
</html>