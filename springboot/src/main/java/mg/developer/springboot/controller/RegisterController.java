package mg.developer.springboot.controller;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import mg.developer.springboot.constant.Role;
import mg.developer.springboot.model.User;
import mg.developer.springboot.service.UserService;

@Controller
public class RegisterController {
	
	@Autowired
	UserService userService;
	
	@Autowired
	BCryptPasswordEncoder passwordEncoder;
	
	@RequestMapping("/register")
	String index(Model model) {
		User user = new User();
		model.addAttribute("user", user);
		return "register/register";
	}
	
	@RequestMapping(value="/register/action", method=RequestMethod.POST)
	ModelAndView addUser(@ModelAttribute("user") User user, RedirectAttributes redirectAttributes) {
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		user.setRole(Role.ROLE_USER.name());
		user.setActive(true);
		try {
			userService.saveOrUpdate(user);
		} catch (DataIntegrityViolationException e) {
			 redirectAttributes.addFlashAttribute("dataIntegrityError", "Erreur d'enregistrement: email "+user.getEmail()+" déjà existant!");
		} catch (ConstraintViolationException e) {
			redirectAttributes.addFlashAttribute("dataIntegrityError", "Address email ou password non valide: password au moins 5 caractères");
		}
		
		return new ModelAndView("redirect:/login");
	}
} 
